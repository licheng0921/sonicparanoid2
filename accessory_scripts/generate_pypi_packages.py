"""
 Create PyPI packages for OSX and LINUX (source).
"""

import os
import sys
from shutil import which, copy
from subprocess import run
import platform
import argparse
from typing import TextIO



########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # define the parameter list
    parser = argparse.ArgumentParser(description=f"Generate python package to be uploaded in PyPI", usage='%(prog)s -o <OUTPUT_DIRECTORY>', prog="generate_pypi_packages.py")

    # Mandatory arguments
    parser.add_argument("-o", "--out-dir", type=str, required=True, help="Output directory.", default=None)
    parser.add_argument("-t", "--make-test-copy", required=False, help="Generate a copy of the package to be used for testing the installation.\n", default=False, action="store_true")

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)




##### MAIN #####
def main() -> None:

    isDarwin = True
    # check OS
    OS = platform.system()
    '''
    if not OS == 'Darwin':
        isDarwin = False
        sys.stderr.write("WARNING: you must use a MacOS system to generate the packages bdist binary package.")
        sys.stderr.write("\nThe MacOS bdist package will not be created.\n")
    '''

    #Get the parameters
    args, parser = get_params()
    del parser
    # Output directory
    outDir: str = os.path.abspath(args.out_dir)
    if not os.path.isdir(outDir):
        os.makedirs(outDir)

    # check python installation
    pythonPath: str = ""
    if which("python3") is None:
        sys.stderr.write("ERROR: the command \"python3\" was not found. Please install python3.")
        sys.exit(-5)
    else:
        pythonPath = which("python3")

    # set log paths
    # get path to the script
    pySrcDir: str = os.path.dirname(os.path.abspath(__file__))
    setupPath: str = os.path.join(os.path.dirname(pySrcDir), "setup.py")
    # check the extans of the setup file
    if not  os.path.isfile(setupPath):
        sys.stderr.write(f"ERROR: the setup file \n{setupPath}\n was not found in the directory\n{pySrcDir}\n")
        sys.exit(-2)


    # generate the sdist source packages
    stdoutPath: str = os.path.join(outDir, "generate_sdist.stdout.txt")
    stderrPath: str = os.path.join(outDir, "generate_sdist.stderr.txt")

    # create the log files
    fdout: TextIO = open(stdoutPath, "w")
    fderr: TextIO = open(stderrPath, "w")
    bldCmd: str = f"{pythonPath} {setupPath} sdist  --dist-dir {outDir}"

    # Print some infomration
    print("\nPyPI package creation info:")
    print(f"OS: {OS}")
    print(f"setup.py path: {setupPath}")
    print(f"Output directory path: {outDir}")
    print(f"Build CMD: {bldCmd}")

    # use run
    run(bldCmd, shell=True, stdout=fdout, stderr=fderr)
    # close the log files
    fdout.close()
    fderr.close()

    # Identify the path with the sdist package
    pkgPath: str = ""
    # print(stdoutPath)
    with open(stdoutPath, "rt") as ifd:
        for ln in ifd:
            # print(ln[:-1])
            # Match the line containing the package name
            if ln.startswith("creating sonicparanoid-"):
                # extract package name
                pkgPath = ln[:-1].split(" ", 1)[-1]
                pkgPath = os.path.join(outDir, f"{pkgPath}.tar.gz")
                # print("\nFOUND!\n")
                break

    if len(pkgPath) == 0:
        sys.stderr.write(f"ERROR: the sdist package was not found inside\n{outDir}!\n")
        sys.exit(-5)

    if args.make_test_copy:
        copy(pkgPath, os.path.join(outDir, "sonicparanoid.test.tar.gz"))
        print(f"\nINFO: a copy of the package for testing was created in the output directory\n{outDir}")

    # print some info on how to upload the packages
    print(f"\nThe package file {os.path.basename(pkgPath)} for PyPI was successfully generated.")
    print(f"To upload it to PyPI run the following command:")
    print(f"twine upload --sign {pkgPath}")
    print("\nor to upload a test version, execute the following:")
    print(f"twine upload --repository-url https://test.pypi.org/legacy/ --sign {pkgPath}")

    print("\nTo install sonicparanoid from the test PyPI repository use the following command:")
    print("pip3 install --index-url https://test.pypi.org/simple/ sonicparanoid")

if __name__ == "__main__":
    main()
