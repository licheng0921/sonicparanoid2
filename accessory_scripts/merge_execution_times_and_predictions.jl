using Distributed
using Base
using Printf: @printf
using ArgParse
using JLD2, FileIO
using Statistics
using StatsBase: describe


#### FUNCTIONS ####
"""Parse the command line arguments and return a dictionary"""
function parse_commandline()
    parserSettings = ArgParseSettings()
    parserSettings.description = "This program does something."
    parserSettings.commands_are_required = false
    parserSettings.version = "1.0"
    parserSettings.add_version = true

    @add_arg_table parserSettings begin
        "--times-c1", "-a"
            help = "Path with the execution times from cicle 1"
            arg_type = String
            required = true
        "--times-c2", "-b"
            help = "Path with the execution times from cicle 2"
            arg_type = String
            required = true
        "--times-ra", "-c"
            help = "Path with the execution times from essentials"
            arg_type = String
            required = true
        "--pred", "-p"
            help = "Table with predictions"
            arg_type = String
            required = true
        "--snapshot", "-s"
            help = "File with species info"
            arg_type = String
            required = true
        "--out-tbl", "-o"
            help = "Master execution times table"
            arg_type = String
            required = true
        "--debug", "-d"
            help = "Print debug lines"
            action = :store_true
    end

    return parse_args(parserSettings)
end



"""Load alignment times"""
function load_align_times(tblPath::String, debug::Bool=false)::Dict{String, Tuple{String, Float64, Float64, Float64}}
    if debug
        println("\nload_align_times :: START")
        @show tblPath
    end

    # output dictionary
    outDict = Dict{String, Tuple{String, Float64, Float64, Float64}}()
    # tmp variables
    tmpTime::Float64 = 0.
    tmpPctA::Float64 = tmpPctB = -1
    a::String = b = tmpPair = ""
    extimes = Array{Float64, 1}()
    reductionsA = Array{Float64, 1}()
    reductionsB = Array{Float64, 1}()

    # parse the input table
    open(tblPath, "r") do ifd
        for ln in eachline(ifd, keep=true)
            # consider alignment, conversion, and parsing time
            # Example line:
            # 49-20 24.720 0.200 0.360 10.29 33.56 0.073
            flds::Array{String, 1} = split(ln, "\t", limit=7)
            tmpPair = flds[1]
            # skip intra species alignments
            a, b = split(tmpPair, "-", limit=2)
            if a == b
                continue
            end

            # consider complete execution time (alignment, conversion, and parsing)
            tmpTime = round(sum([parse(Float64, x) for x in flds[2:4]]), digits=2)
            # add the time to perform the reduction
            tmpTime += round(parse(Float64, flds[7]), digits=2)
            tmpPctA = parse(Float64, flds[5])
            tmpPctB = parse(Float64, flds[6])
            #@show tmpPair
            #@show tmpTime
            if !haskey(outDict, tmpPair)
                outDict[tmpPair] = ("$b-$a", tmpTime, tmpPctA, tmpPctB)
                # add the values to the arrays
                push!(extimes, tmpTime)
                push!(reductionsA, tmpPctA)
                push!(reductionsB, tmpPctB)
                #@show typeof(outDict[tmpPair])
                #@show outDict[tmpPair]
            else
                println("ERROR: pair $(tmpPair) found multiple times!")
                exit(-5)
            end
        end
    end

    if debug
        println("Loaded alignment times:\t$(length(outDict))")
    end

    if debug
        # print some stats
        print("\nExecution times stats")
        describe(extimes)

        print("\nReduction A stats:")
        describe(reductionsA)

        print("\nReduction B stats:")
        describe(reductionsB)
    end

    # retun alignment times
    return outDict
end



"""Load predictions"""
function load_predictions(tblPath::String, debug::Bool=false)::Dict{String, Tuple{Float64, Float64, Int64}}
    if debug
        println("\nload_predictions :: START")
        @show tblPath
    end

    # output dictionary
    outDict = Dict{String, Tuple{Float64, Float64, Int64}}()
    # tmp variables
    tmpPred::Int64 = 0
    tmpSeqDiff::Float64 = tmpSizeDiff = -1000
    a::String = b = tmpPair = ""
    seqDiffArray = Array{Float64, 1}()
    sizeDiffArray = Array{Float64, 1}()
    predArray = Array{Int64, 1}()

    # parse the input table
    ifd::IOStream = open(tblPath, "r")
    # skip the HDR
    readline(ifd, keep=false)
    # Skip the first line
    for ln in eachline(ifd, keep=false)
        # Example line:
        # pair seq_diff_folds_b_gt_a size_diff_folds_b_gt_a size_a size_b avg_len_a avg_len_b pred
        # 1-2 -8.003 -13.322 6514488 488987 524.178 314.866 0
        flds::Array{String, 1} = split(ln, "\t", limit=8)
        tmpPair = flds[1]
        # skip intra species alignments
        a, b = split(tmpPair, "-", limit=2)
        # extract the other information
        tmpSeqDiff = parse(Float64, flds[2])
        tmpSizeDiff = parse(Float64, flds[3])
        tmpPred = parse(Int64, flds[8])
        #@show (tmpPair, tmpSeqDiff, tmpSizeDiff, tmpPred)
        #@show ("$(b)-$(a)", -tmpSeqDiff, -tmpSizeDiff, abs(tmpPred - 1))

        if !haskey(outDict, tmpPair)
            outDict[tmpPair] = (tmpSeqDiff, tmpSizeDiff, tmpPred)
            outDict["$(b)-$(a)"] = (-tmpSeqDiff, -tmpSizeDiff, abs(tmpPred - 1))
            # add the values to the arrays
            push!(predArray, tmpPred)
            push!(seqDiffArray, tmpSeqDiff)
            push!(sizeDiffArray, tmpSizeDiff)
        else
            println("ERROR: pair $(tmpPair) found multiple times!")
            exit(-5)
        end

    end

    close(ifd)


    if debug
        println("Loaded alignment times:\t$(length(outDict))")
    end

    if debug
        # print some stats
        print("\nSequence difference folds")
        describe(seqDiffArray)

        print("\nProteome size difference folds")
        describe(sizeDiffArray)

        print("\nPrediction stats (must be 50/50):")
        describe(predArray)
    end

    # retun alignment times
    return outDict
end



"""Load proteomes info"""
function load_proteomes_info(tblPath::String, debug::Bool=false)::Dict{String, Tuple{Int64, Int64}}
    if debug
        println("\nload_proteomes_info :: START")
        @show tblPath # file with proteomes information
    end

    # output dictionary
    outDict = Dict{String, Tuple{Int64, Int64}}()
    flds = Array{String, 1}()
    species::String = ""
    size::Int64 = proteins = -1

    # parse the file
    open(tblPath, "r") do ifd
        for ln in eachline(ifd)
            # line example
            # 1 anopheles_gambiae.fa 56b8b494a1beee443c4d2f1e96f584bdc59758a98bf798bae1332b558d3fe40c 12428 6514488
            flds = split(ln, "\t", limit=5)
            #@show flds
            #@show ln
            species = flds[1]
            size = parse(Int64, flds[5])
            proteins = parse(Int64, flds[4])
            if !haskey(outDict, species)
                outDict[species] = (proteins, size)
                #@show species
                #@show outDict[species]
            else
                println("ERROR: pair $(species) found multiple times!")
                exit(-5)
            end
        end
    end
    # return the information
    return outDict
end



"""Create table with execution time differences and predictions"""
function create_comparison_table(tblC1::String, tblC2::String, tblRA::String, snapshot::String, tblPred::String, outTbl::String, debug::Bool=false)
    if debug
        println("\ncreate_comparison_table :: START")
        @show tblC1
        @show tblC2
        @show tblRA
        @show snapshot
        @show tblPred
        @show outTbl
    end

    # define main variables
    seqcntA::Int64 = sizeA = seqcntB = sizeB = prediction = fastest = rightpred = -1
    seqDiff::Float64 = sizeDiff = extimeDiff  = tCa1 = tCa2 = tRA = redA = redB = timeDiff_Ca2_RA = -1.
    a::String = b = pair = revpair = rightpred_lab = ""

    # Load align times
    c1times = load_align_times(tblC1, debug)
    c2times = load_align_times(tblC2, debug)
    ratimes = load_align_times(tblRA, debug)

    # load proteome info
    protInfo = load_proteomes_info(snapshot, debug)
    # load predictions
    predictions = load_predictions(tblPred, debug)
    # create the output file and add the headers
    ofd::IOStream = open(outTbl, "w")
    # write the HDR
    write(ofd, "pair\ttime_ca1\ttime_ca2\ttime_ra\treduction_a\treduction_b\tseq_cnt_a\tseq_cnt_b\tsize_a\tsize_b\tseq_diff_folds_b_gt_a\tsize_diff_folds_b_gt_a\ttime_diff_ca1_vs_ca2\ttime_diff_ca2_vs_ra\tprediction\tfastest_pair\tright_pred_int\tright_pred\n")
    #@show ofd

    # extract information from dictionaries
    for (pair, info) in c1times
        revpair, tCa1 = info[1:2]
        #@show pair
        #@show info
        #@show revpair, tCa1

        # skip if it is intraproteomes alignments
        if pair == revpair
            continue
        end

        # extract info for the cicle2 (complete mode slow)
        tCa2 = c2times[revpair][2]
        # extract info for essential alignments
        tRA, redA, redB = ratimes[revpair][2:end]

        # extract proteome info
        a, b = split(pair, "-", limit=2)
        # extract proteome info
        seqcntA, sizeA = protInfo[a]
        seqcntB, sizeB = protInfo[b]
        #@show (a, b, seqcntA, seqcntB, sizeA, sizeB)

        # extract prediction information
        seqDiff, sizeDiff, prediction = predictions[pair]

        # check if pair is really the fastest pair (comparing tCa1 and tCa2)
        tCa1 <= tCa2 ? fastest=1 : fastest=0
        # set the label for the right_prediction field (1 or 0)
        fastest==prediction ? rightpred=1 : rightpred=0
        fastest==prediction ? rightpred_lab="right" : rightpred_lab="wrong"

        # start writing the information
        write(ofd, "$pair\t$tCa1\t$tCa2\t$(round(tRA, digits=3))\t$redA\t$redB\t$seqcntA\t$seqcntB\t$sizeA\t$sizeB\t$seqDiff\t$sizeDiff\t$(round(abs(tCa1-tCa2), digits=3))\t$(round(abs(tCa2-tRA), digits=3))\t$prediction\t$fastest\t$rightpred\t$rightpred_lab\n")
        #break
    end
    close(ofd)
end



#### MAIN START ####

# initialize the command line parameter parser
parsedArgs = parse_commandline()
#println(parsedArgs)

debug = parsedArgs["debug"]
#@show debug

# table with the original execution times
c1 = abspath(parsedArgs["times-c1"])
c2 = abspath(parsedArgs["times-c2"])
ra = abspath(parsedArgs["times-ra"])
snapshot = abspath(parsedArgs["snapshot"])
predTbl = abspath(parsedArgs["pred"])
outTbl = abspath(parsedArgs["out-tbl"])

if debug
    @show c1
    @show c2
    @show ra
    @show snapshot
    @show predTbl
    @show outTbl
end


### MAIN ###

# merge all the information in the new output table
create_comparison_table(c1, c2, ra, snapshot, predTbl, outTbl, debug)