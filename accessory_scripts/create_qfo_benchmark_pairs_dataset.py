"""Split the relation IDs in pairwise relation file"""
import os
import sys
import shutil

########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # create the possible values for sensitivity value
    # define the parameter list
    import argparse
    usage = "\nProvide a file with pairwise ortholog relations\n"
    parser = argparse.ArgumentParser(description="Remove specific alignments",  usage="%(prog)s -i <PAIRWISE RELATION FILE>[options] -p <NEW_FILE_WITH_RELATIONS>", prog="split_qfo11")
    #start adding the command line options
    parser.add_argument("-i", "--input-pairs", type=str, required=True, help="File with relations.", default=None)
    parser.add_argument("-p", "--pairs-file", type=str, required=True, help="File with split pairwise relationships.", default=None)
    # pairwise orthology inference
    parser.add_argument("-r", "--reference-dataset", required=True, help="QfO reference dataset used.", choices=["2011", "2020"], default="2020")


    parser.add_argument("-d", "--debug", required=False, help="Output debug information.", default=False, action="store_true")
    args = parser.parse_args()
    return (args, parser)



########### MAIN ############

def main():

    #Get the parameters
    args, parser = get_params()
    #set the required variables
    debug = args.debug
    # alignments dir
    oldRel = os.path.realpath(args.input_pairs)
    # pairs file
    outPath = os.path.realpath(args.pairs_file)
    # Reference QfO dataset used (e.g., 2020)
    ref:str = args.reference_dataset

    if debug:
        print("Split QfO predictions :: START")
        print(f"Input relationships: {oldRel}")
        print(f"Output relationships: {outPath}")
        print(f"QfO dataset: {ref}")

    # open the output file
    ofd = open(outPath, "wt")
    wcnt: int = 0
    rcnt: int = 0

    a: str = ""
    b: str = ""
    # read the list of pairs
    with open(oldRel, "rt") as fd:
        for ln in fd:
            rcnt += 1
            pair = ln.rstrip("\n")
            a, b = pair.split("\t", 1)
            if ref == "2011":
                ofd.write("{:s}\t{:s}\n".format(a.split("_", 1)[1], b.split("_", 1)[1]))
            elif ref == "2020":
                # Lines in the raw relations file look at follows
                # sp|A0A024RBG1|NUD4B_HUMAN	sp|Q8R2U6|NUDT4_MOUSE    
                a, b = pair.split("\t", 1)
                # Keep only the gene id in the middle
                a = a.split("|", 2)[1]
                b = b.split("|", 2)[1]
                ofd.write(f"{a}\t{b}\n")
            wcnt += 1

    print(f"Read relations:\t{rcnt}")
    print(f"Wrote relations:\t{wcnt}")

if __name__ == "__main__":
    main()
