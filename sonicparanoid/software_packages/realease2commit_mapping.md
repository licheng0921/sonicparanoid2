## Mapping of MMseqs2 release to commits
> The commit can be seen in the first output lines when running under `MMseqs2 Version`

## [MMseqs2 Release 13-45111](https://github.com/soedinglab/MMseqs2/releases/tag/13-45111)
>February 24, 2021  

| SIMD | Commit |
| ----------- | ----------- |
| AVX2_Linux | 45111b641859ed0ddd875b94d6fd1aef1a675b7e |
| SSE4.1_Linux | 45111b641859ed0ddd875b94d6fd1aef1a675b7e | 
| MacOS_Universal | to_check |
| SSE4.1_MacOS | to_check | 


## [MMseqs2 Release 12-113e3](https://github.com/soedinglab/MMseqs2/releases/tag/12-113e3)
>September 1, 2020  

| SIMD | Commit |
| ----------- | ----------- |
| AVX2_Linux | e1a1c1226ef22ac3d0da8e8f71adb8fd2388a249 |
| SSE4.1_Linux | e1a1c1226ef22ac3d0da8e8f71adb8fd2388a249 | 
| MacOS_Universal | to_check |
| SSE4.1_MacOS | to_check | 


## [MMseqs2 Release 11-e1a1c](https://github.com/soedinglab/MMseqs2/releases/tag/11-e1a1c)
>Feb 12, 2020  

| SIMD | Commit |
| ----------- | ----------- |
| AVX2_Linux | e1a1c1226ef22ac3d0da8e8f71adb8fd2388a249 |
| SSE4.1_Linux | e1a1c1226ef22ac3d0da8e8f71adb8fd2388a249 | 
| AVX2_MacOS | to_check |
| SSE4.1_MacOS | to_check | 


## [MMseqs2 Release 10-6d92c](https://github.com/soedinglab/MMseqs2/releases/tag/10-6d92c)
>August 23, 2019  

| SIMD | Commit |
| ----------- | ----------- |
| AVX2_Linux | dc054792d1b1d091380638a712ee7566aba2bb38 |
| SSE4.1_Linux | dc054792d1b1d091380638a712ee7566aba2bb38 | 
| AVX2_MacOS | to_check |
| SSE4.1_MacOS | to_check | 


## Versions used in SonicParanoid
| SonicParanoid | SIMD | Commit |
| ----------- | ----------- | ----------- |
| 1.3.2 | AVX2_Linux | ebb16f3631d320680a306c03aa7412c572f72ee3 |
| 1.3.2 | SSE4.1_Linux | ebb16f3631d320680a306c03aa7412c572f72ee3 |