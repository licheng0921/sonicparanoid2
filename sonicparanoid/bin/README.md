This directory contains the binaries used by SonicParanoid
- [Diamond](https://github.com/bbuchfink/diamond)
- [MMseqs2](https://github.com/soedinglab/mmseqs2)
- [NCBI BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi)
- [MCL](https://micans.org/mcl/)
- Adaboost model

